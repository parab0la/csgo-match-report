var app = angular.module('MatchReport', ['ngValidate', 'ngRoute']);


app.config(function ($routeProvider) {
    $routeProvider
        .when('/leaderboard', {
            title: 'Current world rankings',
            templateUrl: 'views/leaderboard.html',
            controller: 'LeaderboardController'
        })

        .otherwise({redirectTo: "/home"})
});