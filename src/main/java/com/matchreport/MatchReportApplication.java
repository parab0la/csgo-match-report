package com.matchreport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MatchReportApplication {

	public static void main(String[] args) {
		SpringApplication.run(MatchReportApplication.class, args);
	}
}
