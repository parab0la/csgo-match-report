package com.matchreport.models;

import java.util.List;

/**
 * Created by Joel on 2016-10-26.
 */
public class MatchModel {

    public enum MatchResult {
        WIN, LOSS, TIE
    }

    private List<MatchParticipantModel> matchParticipants;
    private MatchResult matchResult;
    private String gameMap;

    public List<MatchParticipantModel> getMatchParticipants() {
        return matchParticipants;
    }

    public void setMatchParticipants(List<MatchParticipantModel> matchParticipants) {
        this.matchParticipants = matchParticipants;
    }

    public MatchResult getMatchResult() {
        return matchResult;
    }

    public void setMatchResult(MatchResult matchResult) {
        this.matchResult = matchResult;
    }

    public String getGameMap() {
        return gameMap;
    }

    public void setGameMap(String gameMap) {
        this.gameMap = gameMap;
    }
}
