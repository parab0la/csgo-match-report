package com.matchreport.repository;

import com.matchreport.models.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Joel on 2016-10-26.
 */
public interface UserRepository extends JpaRepository<UserModel, Long> {

}
